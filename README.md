# SRD Model
## Introduction
SRD (Simple Receive Dispatch) is a **dockerized 3-tier microservices RESTful MVC web application** developed and maintained by [BSPTeams (Sustainable Constructive Engineering)](http://www.bspteams.com/) for [Sardkhaneh Farrokh](https://sardkhaneh.com/). This repository includes Model module of MVC, a SQL representation of some ERD/schema.
## Instructions
This project benefits from a docker ecosystem to compile, run and setup a microservice. Note that the other two modules namely, [SRD Controller](https://gitlab.com/shvahabi/simple-receive-dispatch-controller) and [SRD View](https://gitlab.com/shvahabi/simple-receive-dispatch-view), shall be up and running in their respective containers for SRD Model to function properly. For simplicity, this documentation assumes current working directory is `/home/noema/shahed/gitlab`.
### Prerequisites
1. Pull docker image from dockerhub:
	- `docker pull shvahabi/fullstackdevtool:0.4`
1. Check available networks to docker:
	- `docker network ls`
1. Create network **bsp** if it does not exist:
	- `docker network create --subnet=172.18.0.0/16 bsp`
1. Clone this repository:
	- `git clone -b latest --depth 1 https://gitlab.com/shvahabi/simple-receive-dispatch-model.git /home/noema/shahed/gitlab/simple-receive-dispatch-model`
### Create Database
Create a DB based on provided schema:
- `DB_USERNAME=foo DB_PASSWORD=bar DB_DATE=$(date +%F_%H:%M:%S.%3N); mv /opt/wms/docker/db/DB.mv.db /opt/wms/docker/db/DB_$DB_DATE.mv.db; docker run --rm -v /opt/wms/docker/db:/db -v /home/noema/shahed/gitlab/simple-receive-dispatch-model:/git shvahabi/fullstackdevtool:0.4 /bin/bash -c "java -cp /h2/bin/h2-1.4.199.jar org.h2.tools.RunScript -url jdbc:h2:file:/db/DB -user $DB_USERNAME -password $DB_PASSWORD -script /git/sqlsnippets.sql"`

Change USERNAME and PASSWORD on demand.
### Run
Run docker microservice:
- `docker run --net bsp --ip 172.18.0.10 -d -v /opt/wms/docker/db:/db -p 3000:8082 -p 3001:9092 --name SRD_Model shvahabi/srdm:0.2`
## Single Liner for New Release
Execute the following single line for every new release:
- `docker stop SRD_Model; docker rm -f SRD_Model; sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-model; git clone -b latest --depth 1 https://gitlab.com/shvahabi/simple-receive-dispatch-model.git /home/noema/shahed/gitlab/simple-receive-dispatch-model; sudo chmod -R 777 /home/noema/shahed/gitlab/simple-receive-dispatch-model; DB_USERNAME=foo DB_PASSWORD=bar DB_DATE=$(date +%F_%H:%M:%S.%3N); mv /opt/wms/docker/db/DB.mv.db /opt/wms/docker/db/DB_$DB_DATE.mv.db; docker run --rm -v /opt/wms/docker/db:/db -v /home/noema/shahed/gitlab/simple-receive-dispatch-model:/git shvahabi/fullstackdevtool:0.4 /bin/bash -c "java -cp /h2/bin/h2-1.4.199.jar org.h2.tools.RunScript -url jdbc:h2:file:/db/DB -user $DB_USERNAME -password $DB_PASSWORD -script /git/sqlsnippets.sql"; sudo chmod -R 777 /opt/wms/docker/db; docker run --net bsp --ip 172.18.0.10 -d -v /opt/wms/docker/db:/db -p 3000:8082 -p 3001:9092 --name SRD_Model shvahabi/srdm:0.2`
## Single Liner for Backup/Hardware Restart (Power Outage)
Execute the following single line for DB backaups and/or recover after power failure:
- `docker stop SRD_Model; DB_DATE=$(date +%F_%H:%M:%S.%3N); cp /opt/wms/docker/db/DB.mv.db /opt/wms/docker/db/DB_$DB_DATE.mv.db; docker start SRD_Model;`
## Notes
- Every new release comes with a probably incompatible schema and hence a new empty DB is created whithin `/opt/wms/docker/db/` of host, accessible via `jdbc:h2:/db/DB` URL from DBMS running on same machine, via `jdbc:h2:tcp://172.18.0.10:9092//db/DB` in bsp docker network, via `jdbc:h2:tcp://192.168.1.3:3001//db/DB` in LAN, and via `jdbc:h2:tcp://128.65.181.83:3001//db/DB` elsewhere.
- Everytime new microservice based DBMS instance starts up, automatically backs up last DB. These previous DBs can be accessed simply like the current DB with same DBMS instance.
- Web server container IP:PORT in bsp network is [172.18.0.10:8082](http://172.18.0.10:8082) binded to host as [127.0.0.1:3000](http://127.0.0.1:3000/).
- TCP server container IP:PORT in bsp network is [172.18.0.10:9092](http://172.18.0.10:9092) binded to host as [127.0.0.1:3001](http://127.0.0.1:3001/).
